var express = require('express');
var app = express()
var http = require('http').Server(app);

//Create server socket
var io = require('socket.io').listen(http);

//These two environment variables is needed to get app work in openshift
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8000;
var ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';


//Listen "connection" message from client socket
io.on('connection', function (socket) {
    console.log('Connected');
    //Listen "chat msg" messag from ANY client
    socket.on('chat msg', function (data) {
        console.log('chat msg reveived');
        io.emit('new message', data); 
    });
});

//Make root context router to return index.html
app.get('/', function (req, res) {
    res.sendfile('./view/index.html');
});

//Make chat_scripts.js router to return chat_scripts.js
app.get('/chat_scripts.js', function (req, res) {
    res.sendfile('./lib/chat_scripts.js');
});

//Make jquery-2.1.4.min.js router to return jquery-2.1.4.min.js
app.get('/jquery-2.1.4.min.js', function (req, res) {
    res.sendfile('./lib/jquery-2.1.4.min.js');
});

//Make jquery-2.1.4.min.js router to return jquery-2.1.4.min.js
app.get('/bootstrap.min.js', function (req, res) {
    res.sendfile('./lib/bootstrap.min.js');
});

//Make styles.css router to return styles.css
app.get('/styles.css', function (req, res) {
    res.sendfile('./css/styles.css');
});

//Make styles.css router to return styles.css
app.get('/bootstrap.min.css', function (req, res) {
    res.sendfile('./css/bootstrap.min.css');
});

//Make styles.css router to return styles.css
app.get('/sound.wav', function (req, res) {
    res.sendfile('./css/sound.wav');
});

http.listen(port,ip);