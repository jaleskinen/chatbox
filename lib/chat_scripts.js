window.onload = function (event) {
    //Connect to server with socket

    //Local mode testing, use this
    //var socket = io();
    
    //Openshift testing, use this
    var socket = io.connect("http://chatbox-omatesti.rhcloud.com:8000");
    var input = $("#sound").prop('checked');
    
    //Get messages box parameters, used in message box scrolling
    var myDiv = document.getElementById('messages');
    
    //Ask user name
    var chatName = prompt("Please enter your name");
    
    //Listen "new message" from server
    socket.on('new message', function (data) {
        //Get textarea with id="messages"
        var textArea = document.getElementById('messages');
        textArea.value += data.name + ": " + data.message + "\n";
        
        //Scroll message box if it is full
        myDiv.scrollTop = (myDiv.scrollHeight);
        
        //Play message sound if it is enabled
        input = $("#sound").prop('checked');
        if (data.name != chatName && input === true) {
            buzzer.play();            
        }

    });
    
    //Prevent messages if chatName is not given
    if (chatName === "" || chatName === undefined || chatName === null) {
        alert('Chat name required!');
    } else {
        //Catch messages and route them to server
        $('#chatmessage').submit(function () {

            var msg = document.getElementById('chat_message');
            var dataToServer = {
                name: chatName,
                message: msg.value
            };

            //send message to server
            socket.emit('chat msg', dataToServer);
            $('#chat_message').val('');
            return false;
        });
    }
};